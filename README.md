[![pipeline status](https://git.coop/aptivate/ansible-roles/django-deploy/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/django-deploy/commits/master)

# django-deploy

A role to deploy a django application.

# Requirements

  * https://git.coop/aptivate/ansible-roles/lets-encrypt#requirements
  * https://git.coop/aptivate/ansible-roles/mysql-server#role-variables

# Role Variables

## Mandatory

  * `django_project_name`: The name of the application.

  * `django_deployment_env`: The deployment environment.
    * Should match the settings file that you want to symlink.

  * `django_database_name`: The application database name.

  * `django_database_user`: The application database user.

  * `django_database_password`: The application database user password.

  * `django_project_url`: The Git URL for cloning the application.

  * `django_project_refspec`: The Git identifier for cloning (for example, `master`).

  * `django_secret_key`: The application secret key.

  * `letsencrypt_domain`:  The domain for the applicaion.

  * `apache_template`: The name of the Apache template to use.

## With Defaults

  * `is_initial_deployment`: Whether this is the first deployment or now.
    * Default is `true`. Change to `false` for recurring deployments.
    * This variable helps speed up following deployments by skipping tasks.

  * `with_gcc`: Whether or not to install GCC.
    * Defaults to `true` (needed by mysql_client).

  * `apache_conf_dir`: Where the Apache web server configuration live.
    * Defaults to `/etc/httpd`.

  * `which_services_go_down`: Which services to bring down during a deployment.
    * Defaults to `['httpd']`

  * `db_dumps_dir`: The name of the folder that holds the database dumps.
    * Defaults to `dbdumps`.

# Dependencies

In general, you'll need a few roles depending on your application.

The base line role dependencies seem to be:

  * https://git.coop/aptivate/ansible-roles/apache-httpd
  * https://git.coop/aptivate/ansible-roles/mysql-server

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: apache-httpd

     - role: mysql-server
       mysql_server_version: 5.7
       mysql_root_password: "wzgds/:Kf2,g" # use ansible-vault
       with_mysql_community_client: true
       with_mysql_community_devel: true
       with_mysql_python: true

     - role: django-deploy
       apache_template: templates/https.conf.j2
       django_project_name: internewshid
       django_database_name: internewshid
       django_database_user: internewshid
       django_database_password: "wzgds/:Kf2,g" # use ansible-vault
       django_project_refspec: master
       django_project_url: 'https://git.coop/aptivate/client-projects/internewshid.git'
       django_secret_key: foobar  # use ansible-vault
       letsencrypt_domain: lin-internews-stage.aptivate.org
```

# Testing

We use Linode for our testing here.

You'll need to expose the `LINODE_API_KEY` environment variable for that.

```
$ pipenv install --dev
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
